const db = require('../../db/connection');

exports.getOddsById = id => db('odds').where('id', id);

exports.createOdds = ({ homeWin, awayWin, draw }) => {
  return db('odds')
    .insert({ home_win: homeWin, away_win: awayWin, draw: draw })
    .returning('*');
};
