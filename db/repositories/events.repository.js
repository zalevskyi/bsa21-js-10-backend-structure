const db = require('../../db/connection');

exports.getEventById = id => db('event').where('id', id);

exports.createEvent = ({ type, homeTeam, awayTeam, startAt, oddsId }) =>
  db('event')
    .insert({
      type,
      home_team: homeTeam,
      away_team: awayTeam,
      start_at: startAt,
      odds_id: oddsId,
    })
    .returning('*');

exports.updateScore = (id, score) =>
  db('event').where('id', id).update({ score }).returning('*');
