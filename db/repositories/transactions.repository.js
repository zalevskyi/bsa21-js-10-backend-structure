const db = require('../../db/connection');

exports.createTransaction = ({ userId, cardNumber, amount }) =>
  db('transaction')
    .insert({ user_id: userId, card_number: cardNumber, amount })
    .returning('*');
