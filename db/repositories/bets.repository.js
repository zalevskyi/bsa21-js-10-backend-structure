const db = require('../../db/connection');

exports.createBet = (data, multiplier, eventId) =>
  db('bet')
    .insert({
      ...data,
      multiplier,
      event_id: eventId,
    })
    .returning('*');

exports.updateBetWin = (id, win) =>
  db('bet').where('id', id).update({
    win,
  });

exports.getUnsettledBetsForEvent = eventId =>
  db('bet').where('event_id', eventId).andWhere('win', null);
