const db = require('../../db/connection');

exports.getUserById = id => db('user').where('id', id).returning('*');

exports.createUser = data => db('user').insert(data).returning('*');

exports.updateUser = (id, data) =>
  db('user').where('id', id).update(data).returning('*');

exports.updateBalance = (id, balance) =>
  db('user').where('id', id).update({
    balance,
  });
