const jwt = require('jsonwebtoken');

exports.authorization = (req, res, next) => {
  const token = req.headers['authorization'];
  if (token) {
    try {
      jwt.verify(token.replace('Bearer ', ''), process.env.JWT_SECRET);
      return next();
    } catch (err) {}
  }
  res.status(401).send({ error: 'Not Authorized' });
};

exports.authorizationAdmin = (req, res, next) => {
  const token = req.headers['authorization'];
  const tokenPayload = jwt.verify(
    token.replace('Bearer ', ''),
    process.env.JWT_SECRET,
  );
  if (tokenPayload.type != 'admin') {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  next();
};
