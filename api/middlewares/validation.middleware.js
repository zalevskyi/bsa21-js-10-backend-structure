const { bet } = require('../services/validation-schemas/bets.schemas');
const {
  event,
  eventGet,
  eventScoreUpdate,
} = require('../services/validation-schemas/events.schemas');
const {
  transaction,
} = require('../services/validation-schemas/transactions.schemas');
const {
  user,
  userGet,
  userUpdate,
} = require('../services/validation-schemas/users.schemas');

function validate(validationSchema, req, res, next) {
  const validation = validationSchema.validate(req.body);
  if (validation.error) {
    return res.status(400).send({ error: validation.error.details[0].message });
  }
  next();
}

function validateParams(validationSchema, req, res, next) {
  const validation = validationSchema.validate(req.params);
  if (validation.error) {
    return res.status(400).send({ error: validation.error.details[0].message });
  }
  next();
}

function validateParamsAndBody(paramsSchema, bodySchema, req, res, next) {
  let validation = paramsSchema.validate(req.params);
  if (validation.error) {
    return res.status(400).send({ error: validation.error.details[0].message });
  }
  validation = bodySchema.validate(req.body);
  if (validation.error) {
    return res.status(400).send({ error: validation.error.details[0].message });
  }
  next();
}

exports.validateBet = (req, res, next) => {
  validate(bet, req, res, next);
};

exports.validateEvent = (req, res, next) => {
  validate(event, req, res, next);
};

exports.validateEventScoreUpdate = (req, res, next) => {
  validateParamsAndBody(eventGet, eventScoreUpdate, req, res, next);
};

exports.validateTransaction = (req, res, next) => {
  validate(transaction, req, res, next);
};

exports.validateUser = (req, res, next) => {
  validate(user, req, res, next);
};

exports.validateUserGet = (req, res, next) => {
  validateParams(userGet, req, res, next);
};

exports.validateUserUpdate = (req, res, next) => {
  validateParamsAndBody(userGet, userUpdate, req, res, next);
};
