function errorHandler(err, _req, res, _next) {
  console.log(err);
  res.status(500).send('Internal Server Error');
}

module.exports = errorHandler;
