const Router = require('express');
const jwt = require('jsonwebtoken');
const statEmitter = require('../../helpers/statEmitter');
const { authorization } = require('../middlewares/authorization.middleware');
const {
  getUserById,
  updateBalance,
} = require('../../db/repositories/users.repository');
const { createBet } = require('../../db/repositories/bets.repository');
const { getEventById } = require('../../db/repositories/events.repository');
const { getOddsById } = require('../../db/repositories/odds.repository');
const { validateBet } = require('../middlewares/validation.middleware');

const router = Router();

router.post('/', authorization, validateBet, (req, res) => {
  const token = req.headers['authorization'];
  const tokenPayload = jwt.verify(
    token.replace('Bearer ', ''),
    process.env.JWT_SECRET,
  );
  const userId = tokenPayload.id;

  req.body.event_id = req.body.eventId;
  req.body.bet_amount = req.body.betAmount;
  delete req.body.eventId;
  delete req.body.betAmount;
  req.body.user_id = userId;
  getUserById(userId).then(([user]) => {
    if (!user) {
      res.status(400).send({ error: 'User does not exist' });
      return;
    }
    if (+user.balance < +req.body.bet_amount) {
      return res.status(400).send({ error: 'Not enough balance' });
    }
    getEventById(req.body.event_id).then(([event]) => {
      if (!event) {
        return res.status(404).send({ error: 'Event not found' });
      }
      getOddsById(event.odds_id).then(([odds]) => {
        if (!odds) {
          return res.status(404).send({ error: 'Odds not found' });
        }
        let multiplier;
        switch (req.body.prediction) {
          case 'w1':
            multiplier = odds.home_win;
            break;
          case 'w2':
            multiplier = odds.away_win;
            break;
          case 'x':
            multiplier = odds.draw;
            break;
        }
        createBet(req.body, multiplier, event.id).then(([bet]) => {
          const currentBalance = user.balance - req.body.bet_amount;
          updateBalance(userId, currentBalance).then(() => {
            statEmitter.emit('newBet');
            return res.send({
              ...mapBetFromDB(bet),
              currentBalance,
            });
          });
        });
      });
    });
  });
});

function mapBetFromDB(bet) {
  return {
    id: bet.id,
    eventId: bet.event_id,
    userId: bet.user_id,
    betAmount: bet.bet_amount,
    prediction: bet.prediction,
    multiplier: bet.multiplier,
    win: bet.win,
    createdAt: bet.created_at,
    updatedAt: bet.updated_at,
  };
}

module.exports = router;
