const Router = require('express');
const {
  authorization,
  authorizationAdmin,
} = require('../middlewares/authorization.middleware');

const router = Router();

router.get('/', authorization, authorizationAdmin, (req, res) => {
  res.send(stats);
});

module.exports = router;
