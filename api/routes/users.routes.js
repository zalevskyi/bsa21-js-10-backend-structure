const Router = require('express');
const jwt = require('jsonwebtoken');
const statEmitter = require('../../helpers/statEmitter');
const { authorization } = require('../middlewares/authorization.middleware');
const {
  getUserById,
  createUser,
  updateUser,
} = require('../../db/repositories/users.repository');
const {
  validateUser,
  validateUserGet,
  validateUserUpdate,
} = require('../middlewares/validation.middleware');

const router = Router();

router.get('/:id', validateUserGet, (req, res) => {
  getUserById(req.params.id).then(([result]) => {
    if (!result) {
      res.status(404).send({ error: 'User not found' });
      return;
    }
    return res.send({
      ...result,
    });
  });
});

router.post('/', validateUser, (req, res, next) => {
  req.body.balance = 0;
  createUser(req.body)
    .then(([result]) => {
      result.createdAt = result.created_at;
      delete result.created_at;
      result.updatedAt = result.updated_at;
      delete result.updated_at;
      statEmitter.emit('newUser');
      return res.send({
        ...result,
        accessToken: jwt.sign(
          { id: result.id, type: result.type },
          process.env.JWT_SECRET,
        ),
      });
    })
    .catch(err => {
      if (err.code == '23505') {
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      next(err);
    });
});

router.put('/:id', authorization, validateUserUpdate, (req, res, next) => {
  const token = req.headers['authorization'];
  const tokenPayload = jwt.verify(
    token.replace('Bearer ', ''),
    process.env.JWT_SECRET,
  );
  if (req.params.id !== tokenPayload.id) {
    return res.status(401).send({ error: 'UserId mismatch' });
  }
  updateUser(req.params.id, req.body)
    .then(([result]) => {
      return res.send({
        ...result,
      });
    })
    .catch(err => {
      if (err.code == '23505') {
        console.log(err);
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      next(err);
    });
});

module.exports = router;
