const Router = require('express');
const {
  authorization,
  authorizationAdmin,
} = require('../middlewares/authorization.middleware');
const {
  getUserById,
  updateBalance,
} = require('../../db/repositories/users.repository');
const {
  createTransaction,
} = require('../../db/repositories/transactions.repository');
const { validateTransaction } = require('../middlewares/validation.middleware');

const router = Router();

router.post(
  '/',
  authorization,
  authorizationAdmin,
  validateTransaction,
  (req, res) => {
    getUserById(req.body.userId).then(([user]) => {
      if (!user) {
        return res.status(400).send({ error: 'User does not exist' });
      }
      createTransaction(req.body).then(([transaction]) => {
        const currentBalance = transaction.amount + user.balance;
        updateBalance(req.body.userId, currentBalance).then(() => {
          return res.send({
            ...mapTransactionFromDB(transaction),
            currentBalance,
          });
        });
      });
    });
  },
);

function mapTransactionFromDB(transaction) {
  return {
    id: transaction.id,
    userId: transaction.user_id,
    cardNumber: transaction.card_number,
    amount: transaction.amount,
    createdAt: transaction.created_at,
    updatedAt: transaction.updated_at,
  };
}

module.exports = router;
