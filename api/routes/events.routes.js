const Router = require('express');
const statEmitter = require('../../helpers/statEmitter');
const {
  authorization,
  authorizationAdmin,
} = require('../middlewares/authorization.middleware');
const {
  getUserById,
  updateBalance,
} = require('../../db/repositories/users.repository');
const {
  updateBetWin,
  getUnsettledBetsForEvent,
} = require('../../db/repositories/bets.repository');
const { createOdds } = require('../../db/repositories/odds.repository');
const {
  createEvent,
  updateScore,
} = require('../../db/repositories/events.repository');
const {
  validateEvent,
  validateEventScoreUpdate,
} = require('../middlewares/validation.middleware');

const router = Router();

router.use(authorization, authorizationAdmin);

router.post('/', validateEvent, (req, res) => {
  createOdds(req.body.odds).then(([odds]) => {
    createEvent({
      ...req.body,
      oddsId: odds.id,
    }).then(([event]) => {
      statEmitter.emit('newEvent');
      return res.send({
        ...mapEventFromDB(event),
        odds: mapOddsFromDB(odds),
      });
    });
  });
});

router.put('/:id', validateEventScoreUpdate, (req, res) => {
  getUnsettledBetsForEvent(req.params.id).then(bets => {
    var [w1, w2] = req.body.score.split(':');
    let result;
    if (+w1 > +w2) {
      result = 'w1';
    } else if (+w2 > +w1) {
      result = 'w2';
    } else {
      result = 'x';
    }
    updateScore(req.params.id, req.body.score).then(([event]) => {
      Promise.all(
        bets.map(bet => {
          if (bet.prediction == result) {
            updateBetWin(bet.id, true);
            getUserById(bet.user_id).then(([user]) => {
              const currentBalance =
                user.balance + bet.bet_amount * bet.multiplier;
              return updateBalance(bet.user_id, currentBalance);
            });
          } else if (bet.prediction != result) {
            return updateBetWin(bet.id, false);
          }
        }),
      );
      res.send(mapEventFromDB(event));
    });
  });
});

function mapEventFromDB(event) {
  return {
    id: event.id,
    oddsId: event.odds_id,
    type: event.type,
    homeTeam: event.home_team,
    awayTeam: event.away_team,
    score: event.score,
    startAt: event.start_at,
    createdAt: event.created_at,
    updatedAt: event.updated_at,
  };
}

function mapOddsFromDB(odds) {
  return {
    id: odds.id,
    homeWin: odds.home_win,
    draw: odds.draw,
    awayWin: odds.away_win,
    createdAt: odds.created_at,
    updatedAt: odds.updated_at,
  };
}

module.exports = router;
