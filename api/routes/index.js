const bets = require('./bets.routes');
const events = require('./events.routes');
const health = require('./health.routes');
const stats = require('./stats.routes');
const transactions = require('./transactions.routes');
const users = require('./users.routes');

module.exports = app => {
  app.use('/bets', bets);
  app.use('/events', events);
  app.use('/health', health);
  app.use('/stats', stats);
  app.use('/transactions', transactions);
  app.use('/users', users);
};
