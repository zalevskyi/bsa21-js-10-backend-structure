const joi = require('joi');

const phoneReg = /^\+?3?8?(0\d{9})$/;

exports.user = joi
  .object({
    id: joi.string().uuid(),
    type: joi.string().required(),
    email: joi.string().email().required(),
    phone: joi.string().pattern(phoneReg).required(),
    name: joi.string().required(),
    city: joi.string(),
  })
  .required();

exports.userUpdate = joi
  .object({
    email: joi.string().email(),
    phone: joi.string().pattern(phoneReg),
    name: joi.string(),
    city: joi.string(),
  })
  .required();

exports.userGet = joi
  .object({
    id: joi.string().uuid(),
  })
  .required();
