const express = require('express');
const routes = require('./api/routes/index');
const db = require('./db/connection');
const statEmitter = require('./helpers/statEmitter');
const errorHandlerMiddleware = require('./api/middlewares/error-handler.middleware');

const app = express();

const port = 3000;

stats = {
  totalUsers: 3,
  totalBets: 1,
  totalEvents: 1,
};

app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db.raw('select 1+1 as result')
    .then(function () {
      neededNext();
    })
    .catch(() => {
      throw new Error('No db connection');
    });
});

routes(app);

app.use(errorHandlerMiddleware);

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
